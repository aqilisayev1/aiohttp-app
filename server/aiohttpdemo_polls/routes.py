from aiohttp import web
from views import index, poll, vote


def setup_routes(app: web.Application):
    app.router.add_get("/", index)
    app.router.add_get("/poll/{question_id}", poll, name="poll")

    app.router.add_post('/poll/{question_id}/vote', vote, name='vote')  # router.add_post(path, handler, **kwargs)
